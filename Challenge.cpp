#include "pch.h" // use stdafx.h in Visual Studio 2017 and earlier
#include <utility>
#include <limits.h>
#include <iostream>
#include "Challenge.h"

using namespace std;

// ������/��������� �������
void homeWork14()
{
	extern const int a, b, c, d;
	float cppStyle = a * (b + (static_cast<float>(c) / d));
	float cStyle = a * (b + (float(c) / d));

	cout << cppStyle << endl;
	cout << cStyle << endl;
}

// ������ �������
void homeWork2()
{
	int num = 21;

	cout << (num <= 21 ? -1 : 2) * (num - 21) << endl;
}

// ������ �������
void homeWork3()
{
	int* pArr = nullptr;
	int arr[3][3][3] = {
		{
			{1, 2, 3},
			{4, 5, 6},
			{7, 8, 9},
		},
		{
			{10, 11, 12},
			{13, 14, 15},
			{16, 17, 18},
		},
		{
			{19, 20, 21},
			{22, 23, 24},
			{25, 26, 27},
		},
	};

	pArr = &arr[1][1][1];

	cout << *pArr << endl;
	cout << pArr[-5] << endl;
}